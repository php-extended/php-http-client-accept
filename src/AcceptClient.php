<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\MimeType\MimeTypeProviderInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * AcceptClient class file.
 * 
 * This class is an implementation of a client which adds accept headers on
 * oncoming requests.
 * 
 * @author Anastaszor
 */
class AcceptClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The mime type provider.
	 * 
	 * @var MimeTypeProviderInterface
	 */
	protected MimeTypeProviderInterface $_mimeTypeProvider;
	
	/**
	 * Builds a new AcceptClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param MimeTypeProviderInterface $mimeTypeProvider
	 */
	public function __construct(ClientInterface $client, MimeTypeProviderInterface $mimeTypeProvider)
	{
		$this->_client = $client;
		$this->_mimeTypeProvider = $mimeTypeProvider;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if(!$request->hasHeader('Accept'))
		{
			$extension = (string) \pathinfo($request->getUri()->getPath(), \PATHINFO_EXTENSION);
			if(empty($extension))
			{
				$extension = 'html';
			}
			
			$mimeTypes = $this->_mimeTypeProvider->getMimeTypesForExtension($extension);
			
			$qty = 1.0;
			$chain = new AcceptChain();
			
			foreach($mimeTypes as $mimeType)
			{
				$chain->addItem(new AcceptItem($mimeType, $qty));
				$qty = \round($qty * 0.9, 2);
			}
			$chain->addItem(new AcceptItem($this->_mimeTypeProvider->getBestMatchMimeType('*/*'), $qty));
			
			try
			{
				$request = $request->withHeader('Accept', $chain->getHeaderValue());
			}
			// @codeCoverageIgnoreStart
			catch(InvalidArgumentException $e)
			// @codeCoverageIgnoreEnd
			{
				// nothing to do
			}
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
