<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use ArrayIterator;
use Countable;
use Iterator;
use IteratorAggregate;
use Stringable;

/**
 * AcceptChain class file.
 * 
 * This class represents the chain of accept items.
 * 
 * @author Anastaszor
 * @implements \IteratorAggregate<int, AcceptItem>
 */
class AcceptChain implements Countable, IteratorAggregate, Stringable
{
	
	/**
	 * The items.
	 * 
	 * @var array<integer, AcceptItem>
	 */
	protected array $_items = [];
	
	/**
	 * Builds a new AcceptChain with the given items.
	 * 
	 * @param array<integer, AcceptItem> $items
	 */
	public function __construct(array $items = [])
	{
		$this->addItems($items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getHeaderValue();
	}
	
	/**
	 * Adds all the items to the chain.
	 * 
	 * @param array<integer, AcceptItem> $items
	 * @return AcceptChain
	 */
	public function addItems(array $items) : AcceptChain
	{
		foreach($items as $item)
		{
			$this->addItem($item);
		}
		
		return $this;
	}
	
	/**
	 * Adds an item to the chain.
	 * 
	 * @param AcceptItem $item
	 * @return AcceptChain
	 */
	public function addItem(AcceptItem $item) : AcceptChain
	{
		foreach($this->_items as $known)
		{
			if($known->getMimeType()->equals($item->getMimeType()))
			{
				return $this;
			}
		}
		
		$this->_items[] = $item;
		
		return $this;
	}
	
	/**
	 * Gets the header value of the chain.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string
	{
		return \implode(',', \array_map(function(AcceptItem $item)
		{
			return $item->getHeaderValue();
		}, $this->_items));
	}
	
	/**
	 * Gets whether this chain is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool
	{
		return 0 === \count($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorAggregate::getIterator()
	 * @return Iterator<AcceptItem>
	 */
	public function getIterator() : Iterator
	{
		return new ArrayIterator($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_items);
	}
	
}
