<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\MimeType\MimeTypeInterface;
use Stringable;

/**
 * AcceptItem class file.
 * 
 * This class represents an item for the accept chain.
 * 
 * @author Anastaszor
 */
class AcceptItem implements Stringable
{
	
	/**
	 * The mime type.
	 * 
	 * @var MimeTypeInterface
	 */
	protected MimeTypeInterface $_mimeType;
	
	/**
	 * The q-value.
	 * 
	 * @var float
	 */
	protected float $_qvalue = 1.0;
	
	/**
	 * Builds a new AcceptItem with the given mime type and q-value.
	 * 
	 * @param MimeTypeInterface $mimeType
	 * @param float $qvalue
	 */
	public function __construct(MimeTypeInterface $mimeType, float $qvalue = 1.0)
	{
		$this->_mimeType = $mimeType;
		$this->_qvalue = $qvalue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getHeaderValue();
	}
	
	/**
	 * Gets the mime type of this item.
	 * 
	 * @return MimeTypeInterface
	 */
	public function getMimeType() : MimeTypeInterface
	{
		return $this->_mimeType;
	}
	
	/**
	 * Gets the q-value of this item.
	 * 
	 * @return float
	 */
	public function getQValue() : float
	{
		return $this->_qvalue;
	}
	
	/**
	 * Gets the header value of this accept item.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string
	{
		if(1.0 === $this->getQValue())
		{
			return $this->getMimeType()->getCanonicalRepresentation();
		}
		
		return $this->getMimeType()->getCanonicalRepresentation().';q='.((string) $this->getQValue());
	}
	
}
