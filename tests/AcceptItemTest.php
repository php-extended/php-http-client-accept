<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\AcceptItem;
use PhpExtended\MimeType\MimeCategory;
use PhpExtended\MimeType\MimeType;
use PHPUnit\Framework\TestCase;

/**
 * AcceptItemTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\AcceptItem
 *
 * @internal
 *
 * @small
 */
class AcceptItemTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptItem
	 */
	protected AcceptItem $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('category/type;q=0.81', $this->_object->__toString());
	}
	
	public function testGetMimeType() : void
	{
		$expected = new MimeType(new MimeCategory('category'), 'type', []);
		$this->assertEquals($expected, $this->_object->getMimeType());
	}
	
	public function testGetQValue() : void
	{
		$this->assertEquals(0.81, $this->_object->getQValue());
	}
	
	public function testGetHeaderValue() : void
	{
		$this->assertEquals('category/type;q=0.81', $this->_object->getHeaderValue());
	}
	
	public function testGetHeaderValueSimple() : void
	{
		$this->assertEquals('category/type', (new AcceptItem(new MimeType(new MimeCategory('category'), 'type', [])))->getHeaderValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AcceptItem(new MimeType(new MimeCategory('category'), 'type', []), 0.81);
	}
	
}
