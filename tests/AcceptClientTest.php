<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\HttpClient\AcceptClient;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\Uri;
use PhpExtended\MimeType\ApacheMimeTypeProvider;
use PhpExtended\MimeType\IanaMimeTypeProvider;
use PhpExtended\MimeType\MimeTypeProvider;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AcceptClientClient implements ClientInterface
{
	
	public $request;
	
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$this->request = $request;
		
		return new Response();
	}
	
}

/**
 * AcceptClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\AcceptClient
 *
 * @internal
 *
 * @small
 */
class AcceptClientTest extends TestCase
{
	
	/**
	 * The client to help.
	 *
	 * @var AcceptClientClient
	 */
	protected AcceptClientClient $_client;
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptClient
	 */
	protected AcceptClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testAddHeader() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('http', null, new Domain('foobar.example.com')));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('text/html,*/*;q=0.9', $this->_client->request->getHeaderLine('Accept'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$provider = new MimeTypeProvider([], []);
		$provider->absorbProvider(new ApacheMimeTypeProvider());
		$provider->absorbProvider(new IanaMimeTypeProvider());
		
		$this->_client = new AcceptClientClient();
		
		$this->_object = new AcceptClient($this->_client, $provider);
	}
	
}
