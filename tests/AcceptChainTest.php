<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-accept library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\AcceptChain;
use PhpExtended\HttpClient\AcceptItem;
use PhpExtended\MimeType\MimeCategory;
use PhpExtended\MimeType\MimeType;
use PHPUnit\Framework\TestCase;

/**
 * AcceptChainTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\AcceptChain
 *
 * @internal
 *
 * @small
 */
class AcceptChainTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptChain
	 */
	protected AcceptChain $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('category/type', $this->_object->__toString());
	}
	
	public function testAddItemSame() : void
	{
		$this->_object->addItem(new AcceptItem(new MimeType(new MimeCategory('category'), 'type', [])));
		$this->assertEquals(1, $this->_object->count());
	}
	
	public function testGetHeaderValue() : void
	{
		$this->assertEquals('category/type', $this->_object->getHeaderValue());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testGetIterator() : void
	{
		$expected = new ArrayIterator([
			new AcceptItem(new MimeType(new MimeCategory('category'), 'type', [])),
		]);
		$this->assertEquals($expected, $this->_object->getIterator());
	}
	
	public function testGetCount() : void
	{
		$this->assertEquals(1, $this->_object->count());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AcceptChain([
			new AcceptItem(new MimeType(new MimeCategory('category'), 'type', [])),
		]);
	}
	
}
